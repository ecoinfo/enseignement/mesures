# Supports enseignants pour ressource R5A08 "Qualité de dév." en BUT3 (CM + TPs), IUT Info Aix

Chères et chers écoinformaticiennes et -ciens,

j'aimerais partager avec vous la V1 de mes supports de cours de "Qualité de dév" en BUT3, expérimentés cette année. Ils se concentrent sur le profiling avec différentes mesures logicielles, dont l'écoIndex et une mesure d'impact du HPC (celle de Green Algorithms).
Je mets tout en public sur Framagit.

Je ne fais pas le tri entre les parties éco et non-éco, pour situer le contexte.
C'est un module (i.e une "ressource", en jargon BUT) très court : 4h de CM + 16 h de TP.
En BUT2 je leur ai déjà fait une intro générale à l'impact environnemental du numérique, et à l'écoconception web (mais pas encore réussi à faire de TPs). Éventuellement je pourrais ajouter les supports aussi.

Le projet git est indépendant de mon cours. On peut le faire évoluer dans plusieurs directions, avec ceux que ça intéressent (ajouts de ressources de chacuns, évolution des supports vers un/des noyaux communs "génériques" et réutilisables, squelettes de TD-TPs ou "clés en main", etc.). J'ai ouvert un wiki pour ça.

## Perspectives d'amélioration (pour ce cours)
 - donner plus, et de meilleurs, indicateurs d'interprétation des mesures
 - confronter les mesures théoriques et prédictives aux mesures appliquées. Par ex. en intégrant un TP sur une grille de calcul comme le grid'5000 (merci Laurent Lefèvre et Julien Lefèvre), ou d'autres sujets vus passer sur la liste ecoinfo-formation@groupes.renater.fr.
 - faire le lien entre les mesures et les bonnes pratiques connues d'écoconception, comme celles de (Bordage, 2022).

Retours bienvenus.

## Où trouver quoi
Normalement les noms de dossiers et fichiers sont à peu près auto-suffisants.

Pour les TDs, un seul fichier ODS contient toutes les formules (y compris les métriques d'écoinfo). Il dans TD-TP/TD-mesures/.

L'examen se fait sur moodle, avec un questionnaire automatisé.

