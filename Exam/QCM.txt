IDÉES EN VRAC DE QUESTIONS POUR QCM MOODLE
==========================================

- Qu'est-ce que l'ADEME ?

- Qu'est-ce que l'arcep ?

- Qu'appelle-t-on une transformation, en théorie de la mesure ?

- Parmi les relations suivantes, lesquelles sont des relations de transformation valides :
 * [0..13] -> A
 
- Parmi les transformations suivantes, lesquelles ne sont pas admissibles ?

- Parmi les échelles suivantes, lesquelles sont nominales ?

- Parmi les échelles suivantes, lesquelles sont ordinales ?

*************

- Quelle est la valeur de complexité cyclomatique du code suivant :
 * block sort ?

- Calculer la complexité selon les métriques de Halstead pour le code suivant :

- Mesurer l'absence de cohésion (LCOM) pour la classe suivante. Cette classe est-elle cohésive ?

- faire deviner une valeur de HV, Volume d'Ahlstead, pour une mesure donnée de MI ?
    
****
Quelle est l'empreinte carbone C du HPC de prévisions météorologiques exécuté dans la configuration suivante :

    {nbProc} processeurs Intel Xeon E5-2695 v4
    chaque processeur est doté de {nbCoeurParProc} coeurs
    chaque processeur est doté d'une mémoire de {mem} Go (soit {mem}*{nbProc} au total)
    le TDP du processeur est de {tdp} W
    le temps d'exécution est de {t} min
    le facteur d'utilisation des coeurs est de {uc}
    la puissance de consommation de la mémoire est de {Pm} W/Go
    le coefficient d'efficacité (PUE) du calculateur est la valeur mondiale moyenne
    l'intensité carbone (CI) est de {ci}
    
RÉPONSE (format moodle) :
({t}/60)*({nbProc}*{nbCoeurParProc} * ({tdp}/{nbCoeurParProc}) * {uc} + {mem}*{nbProc}*{Pm})*{pue}*{ci}*0.001
















